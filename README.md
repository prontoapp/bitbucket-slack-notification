# BitBucket-Slack notification #

### What is this repository for? ###

* Receive webhook from BitBucket
* Send webhook to Slack

### Supported messages ###

* Comments
  * Created on commit
  * Created on PR
* Pull Requests
  * Created
  * Merged

### Run ###

```docker run -e SLACK_WEBHOOK=YOUR_SLACK_WEBHOOK_URL -e URL_SECRET=yoursecretpassword  lukaspronto/bitbucket-slack-pr-hook```

Setup BitBucket webhook to ```http://yourdomain/yoursecretpassword```