class BitbucketWebhookSlackFormatter

  class BitbucketEventFormatter
    def initialize(event)
      @event = event
    end
  end

  class BigNotificationFormatter < BitbucketEventFormatter
    def slack_attachment
      {
        pretext: summary,
        text: description,
        title: title,
        title_link: title_link
      }
    end
  end

  class SmallNotificationFormatter < BitbucketEventFormatter
    def slack_attachment
      {
        pretext: summary,
        text: description
      }
    end
  end

  class LineNotificationFormatter < BitbucketEventFormatter
    def slack_attachment
      {
        text: summary
      }
    end
  end


  class PullrequestCommentCreatedFormatter < SmallNotificationFormatter
    def summary
      "#{@event.actor.display_name} commented on pull request \"<#{@event.comment.links.html}|#{@event.pullrequest.title}>\":"
    end

    def description
      @event.comment.content_raw
    end
  end

  class PullrequestCommentUpdatedFormatter < SmallNotificationFormatter
    def summary
      "#{@event.actor.display_name} updated comment on pull request \"<#{@event.comment.links.html}|#{@event.pullrequest.title}>\":"
    end

    def description
      @event.comment.content_raw
    end
  end

  class PullrequestCommentDeletedFormatter < SmallNotificationFormatter
    def summary
      "#{@event.actor.display_name} deleted comment on pull request \"<#{@event.pullrequest.links.html}|#{@event.pullrequest.title}>\":"
    end

    def description
      @event.comment.content_raw
    end
  end

  class PullrequestCreatedFormatter < BigNotificationFormatter
    def summary
      "#{@event.actor.display_name} created new pull request"
    end

    def title
      @event.pullrequest.title
    end

    def title_link
      @event.pullrequest.links.html
    end

    def description
      @event.pullrequest.description
    end
  end

  class PullrequestApprovedFormatter < LineNotificationFormatter
    def summary
      "#{@event.actor.display_name} approved pull request \"<#{@event.pullrequest.links.html}|#{@event.pullrequest.title}>\""
    end
  end

  class PullrequestUnapprovedFormatter < LineNotificationFormatter
    def summary
      "#{@event.actor.display_name} removed approval pull request \"<#{@event.pullrequest.links.html}|#{@event.pullrequest.title}>\""
    end
  end

  class PullrequestFulfilledFormatter < SmallNotificationFormatter
    def summary
      "Pull request \"<#{@event.pullrequest.links.html}|#{@event.pullrequest.title}>\" merged by #{@event.actor.display_name}"
    end

    def description
      @event.pullrequest.description
    end
  end

  class PullrequestUpdatedFormatter < SmallNotificationFormatter
    def summary
      "#{@event.actor.display_name} updated pull request \"<#{@event.pullrequest.links.html}|#{@event.pullrequest.title}>\":"
    end

    def description
      @event.pullrequest.description
    end
  end

  class PullrequestRejectedFormatter < SmallNotificationFormatter
    def summary
      "#{@event.actor.display_name} rejected pull request \"<#{@event.pullrequest.links.html}|#{@event.pullrequest.title}>\":"
    end

    def description
      @event.pullrequest.reason
    end
  end

  class CommitCommentCreatedFormatter < SmallNotificationFormatter
    def summary
      "#{@event.actor.display_name} commented on commit <#{@event.comment.links.html}|#{@event.commit_message}>"
    end

    def description
      @event.comment.content_raw
    end
  end

  class GenericFormatter < LineNotificationFormatter
    def summary
      @event.to_s
    end
  end

  FORMATTER_MAP = {
    Bitbucket::WebhookEvents::Pullrequest::CommentCreated => PullrequestCommentCreatedFormatter,
    Bitbucket::WebhookEvents::Pullrequest::CommentUpdated => PullrequestCommentUpdatedFormatter,
    Bitbucket::WebhookEvents::Pullrequest::CommentDeleted => PullrequestCommentDeletedFormatter,
    Bitbucket::WebhookEvents::Pullrequest::Created => PullrequestCreatedFormatter,
    Bitbucket::WebhookEvents::Pullrequest::Updated => PullrequestUpdatedFormatter,
    Bitbucket::WebhookEvents::Pullrequest::Approved => PullrequestApprovedFormatter,
    Bitbucket::WebhookEvents::Pullrequest::Rejected => PullrequestRejectedFormatter,
    Bitbucket::WebhookEvents::Pullrequest::Unapproved => PullrequestUnapprovedFormatter,
    Bitbucket::WebhookEvents::Pullrequest::Fulfilled => PullrequestFulfilledFormatter,
    Bitbucket::WebhookEvents::Repo::CommitCommentCreated => CommitCommentCreatedFormatter
  }

  def self.attachment(event)
    formatter_class = FORMATTER_MAP[event.class] || GenericFormatter

    formatter = formatter_class.new(event)
    formatter.slack_attachment
  end

end
