require 'json'
require 'active_support/core_ext/string'

module Bitbucket
  # When you have a webhook with an event, Bitbucket sends the event request
  # to the server URL for the webhook whenever that event occurs. This page
  # describes the structure of these requests.
  class WebhookEvent

    def self.parse(json, event_key)
      event_category, event_name = event_key.split(':')

      event_class_name = "bitbucket/webhook_events/#{event_category}/#{event_name}".classify
      json_object = JSON.parse(json)
      event_class_name.constantize.new(json_object)
    end

    def initialize(json)
      @raw = json
    end
  end

end
