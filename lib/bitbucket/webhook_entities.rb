require_relative 'webhook_entities/user'
require_relative 'webhook_entities/repository'
require_relative 'webhook_entities/issue'
require_relative 'webhook_entities/comment'
require_relative 'webhook_entities/pull_request'
require_relative 'webhook_entities/links'


module Bitbucket
  module WebhookEntities
  end
end
