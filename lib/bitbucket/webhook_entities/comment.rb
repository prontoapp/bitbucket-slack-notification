module Bitbucket
  module WebhookEntities
    # Comment
    # The Comment in event payloads. In addition to the following fields,
    # the comment entity may include other fields as well.
    class Comment

      attr_accessor :id, :parent, :created_on, :updated_on, :links

      def initialize(raw)
        @raw = raw
        self.id = @raw['id']
        self.parent = @raw['parent']
        self.created_on = DateTime.parse(@raw['created_on'])
        self.updated_on = @raw['updated_on'] && DateTime.parse(@raw['updated_on'])
        self.links = Bitbucket::WebhookEntities::Links.new(@raw['links'])
      end

      def content_markup
        @raw['content']['markup']
      end

      def content_html
        @raw['content']['html']
      end

      def content_raw
        @raw['content']['raw']
      end

      def inline?
        !@raw['inline'].nil?
      end

      def file_path
        @raw['inline'] && @raw['inline']['path']
      end

      def file_line_green
        @raw['inline'] && @raw['inline']['to']
      end

      def file_line_red
        @raw['inline'] && @raw['inline']['from']
      end

      def file_link
        @raw['links']['code'] && @raw['links']['code']['href']
      end

      def link
        links.html
      end

      def to_s
        "#{content_html} (#{link})"
      end
    end
  end
end
