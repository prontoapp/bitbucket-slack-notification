module Bitbucket
  module WebhookEntities
    # Repository
    # The Repository in event payloads. In addition to the following fields,
    # the repository entity may include other fields as well.
    class Repository

      attr_accessor :name, :full_name, :uuid, :links, :scm, :is_private

      def initialize(raw)
        @raw = raw
        self.name = @raw['name']
        self.full_name = @raw['full_name']
        self.scm = @raw['scm']
        self.is_private = @raw['is_private']
        self.links = Bitbucket::WebhookEntities::Links.new(@raw['links'])
      end

      def to_s
        "#{full_name} (#{links.html})"
      end
    end
  end
end
