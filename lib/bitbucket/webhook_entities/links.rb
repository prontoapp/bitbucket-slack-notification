module Bitbucket
  module WebhookEntities
    # Common links entity
    class Links

      attr_accessor :html, :api, :avatar

      def initialize(json)
        @raw = json
        self.api = json['self']['href']
        self.html = json['html'] && json['html']['href']
        self.avatar = json['avatar'] && json['avatar']['href']
      end
    end
  end
end
