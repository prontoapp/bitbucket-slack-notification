module Bitbucket
  module WebhookEntities
    # Issue
    # The Issue in event payloads. In addition to the following fields,
    # the issue entity may include other fields as well.
    class Issue
      attr_accessor :id, :component, :title, :content, :priority, :state, :type,
        :milestone, :version, :created_on, :updated_on, :links

      def initialize(raw)
        @raw = raw

        self.id = @raw['id']
        self.component = @raw['component']
        self.title = @raw['title']
        self.priority = @raw['priority']
        self.state = @raw['state']
        self.type = @raw['type']
        self.milestone = @raw['milestone'] && @raw['milestone']['name']
        self.version = @raw['version'] && @raw['version']['name']
        self.created_on = DateTime.parse(@raw['created_on'])
        self.updated_on = @raw['updated_on'] && DateTime.parse(@raw['updated_on'])
        self.links = Bitbucket::WebhookEntities::Links.new(@raw['links'])
      end

      def content_markup
        @raw['content']['markup']
      end

      def content_html
        @raw['content']['html']
      end

      def content_raw
        @raw['content']['raw']
      end

      def to_s
        "#{title} (#{links.html})"
      end
    end
  end
end
