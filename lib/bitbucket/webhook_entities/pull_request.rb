module Bitbucket
  module WebhookEntities
    # Pull Request
    # The Pull Request in event payloads. In addition to the following fields,
    # the pull request entity may include other fields as well.
    class PullRequest
      attr_accessor :id
      attr_accessor :title
      attr_accessor :description
      attr_accessor :state
      attr_accessor :author
      attr_accessor :source
      attr_accessor :destination
      attr_accessor :merge_commit
      attr_accessor :participants
      attr_accessor :reviewers
      attr_accessor :close_source_branch
      attr_accessor :closed_by
      attr_accessor :reason
      attr_accessor :created_on
      attr_accessor :updated_on
      attr_accessor :links

      class Endpoint
        attr_accessor :branch, :commit, :repository
        def initialize(json)
          self.branch = json['branch']['name']
          self.commit = json['commit']['hash']
          self.repository = Bitbucket::WebhookEntities::Repository.new(json['repository'])
        end
      end

      def initialize(json)
        @raw = json

        self.id = @raw['id']
        self.title = @raw['title']
        self.description = @raw['description']
        self.state = @raw['state']
        self.author = Bitbucket::WebhookEntities::User.new(@raw['author'])
        self.source = Endpoint.new(@raw['source'])
        self.destination = Endpoint.new(@raw['destination'])
        self.merge_commit = @raw['merge_commit'] && @raw['merge_commit']['hash']
        self.participants = @raw['participants'].map { |p|
          Bitbucket::WebhookEntities::User.new(p['user'])
        }
        self.reviewers = @raw['reviewers'].map { |p|
          Bitbucket::WebhookEntities::User.new(p)
        }
        self.close_source_branch = @raw['close_source_branch?']
        self.closed_by = @raw['closed_by'] && Bitbucket::WebhookEntities::User.new(@raw['closed_by'])
        self.reason = @raw['reason']
        self.created_on = DateTime.parse(@raw['created_on'])
        self.updated_on = @raw['updated_on'] && DateTime.parse(@raw['updated_on'])
        self.links = Bitbucket::WebhookEntities::Links.new(@raw['links'])
      end

      def to_s
        "#{title} (#{links.html})"
      end
    end
  end
end
