module Bitbucket
  module WebhookEntities
    # User
    # The User in event payloads. In addition to the following fields, the user
    # entity may include other fields as well.
    class User

      attr_accessor :username, :display_name, :uuid, :links

      def initialize(raw)
        @raw = raw
        self.username = @raw['username']
        self.display_name = @raw['display_name']
        self.uuid = @raw['uuid']
        self.links = Bitbucket::WebhookEntities::Links.new(@raw['links'])
      end

      def to_s
        "#{display_name} (#{links.html})"
      end
    end
  end
end
