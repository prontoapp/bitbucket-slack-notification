require 'json'

module Bitbucket
  module WebhookEvents
    module Pullrequest
      # Base for Comment events on Pull Request
      class CommentBase < Bitbucket::WebhookEvents::Pullrequest::Base
        attr_accessor :comment

        def initialize(raw)
          super(raw)

          self.comment = Bitbucket::WebhookEntities::Comment.new(@raw['comment'])
        end
      end
    end
  end
end
