require_relative 'base'

module Bitbucket
  module WebhookEvents
    module Pullrequest
      # Pull Request Merged
      class Fulfilled < Bitbucket::WebhookEvents::Pullrequest::Base
        def to_s
          "#{actor} merged PR #{pullrequest} to #{pullrequest.destination.branch}"
        end
      end
    end
  end
end
