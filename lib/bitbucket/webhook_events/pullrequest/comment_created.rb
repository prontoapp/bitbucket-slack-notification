require_relative 'comment_base'

module Bitbucket
  module WebhookEvents
    module Pullrequest
      # Commit Comment Created
      # A user comments on a commit in a pull request.
      class CommentCreated < Bitbucket::WebhookEvents::Pullrequest::CommentBase
        def to_s
          "#{actor} commented on PR #{pullrequest}: \"#{comment}\""
        end
      end
    end
  end
end
