require_relative 'base'

module Bitbucket
  module WebhookEvents
    module Pullrequest
      # Pull Request Updated
      class Updated < Bitbucket::WebhookEvents::Pullrequest::Base
        def to_s
          "#{actor} updated PR #{pullrequest}"
        end
      end
    end
  end
end
