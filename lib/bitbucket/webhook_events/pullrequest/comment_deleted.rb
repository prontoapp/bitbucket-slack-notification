require_relative 'comment_base'

module Bitbucket
  module WebhookEvents
    module Pullrequest
      # Commit Comment Deleted
      # A user comments on a commit in a pull request.
      class CommentDeleted < Bitbucket::WebhookEvents::Pullrequest::CommentBase
        def to_s
          "#{actor} deleted comment on PR #{pullrequest}: \"#{comment}\""
        end
      end
    end
  end
end
