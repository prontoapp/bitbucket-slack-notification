require_relative 'base'

module Bitbucket
  module WebhookEvents
    module Pullrequest
      # Pull Request Approved
      class Unapproved < Bitbucket::WebhookEvents::Pullrequest::Base
        def to_s
          "#{actor} removed approval for PR #{pullrequest}"
        end
      end
    end
  end
end
