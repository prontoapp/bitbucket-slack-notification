require_relative 'comment_base'

module Bitbucket
  module WebhookEvents
    module Pullrequest
      # Commit Comment Updated
      # A user comments on a commit in a pull request.
      class CommentUpdated < Bitbucket::WebhookEvents::Pullrequest::CommentBase
        def to_s
          "#{actor} updated comment on PR #{pullrequest}: \"#{comment}\""
        end
      end
    end
  end
end
