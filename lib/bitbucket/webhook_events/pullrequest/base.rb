module Bitbucket
  module WebhookEvents
    module Pullrequest
      # Base class for Pull Request Events
      class Base < Bitbucket::WebhookEvent
        attr_accessor :actor, :repository, :pullrequest

        def initialize(raw)
          super(raw)

          self.actor = Bitbucket::WebhookEntities::User.new(@raw['actor'])
          self.repository = Bitbucket::WebhookEntities::Repository.new(@raw['repository'])
          self.pullrequest = Bitbucket::WebhookEntities::PullRequest.new(@raw['pullrequest'])
        end
      end
    end
  end
end
