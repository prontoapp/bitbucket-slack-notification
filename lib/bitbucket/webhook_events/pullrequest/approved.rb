require_relative 'base'

module Bitbucket
  module WebhookEvents
    module Pullrequest
      # Pull Request Approved
      class Approved < Bitbucket::WebhookEvents::Pullrequest::Base
        def to_s
          "#{actor} approved PR #{pullrequest}"
        end
      end
    end
  end
end
