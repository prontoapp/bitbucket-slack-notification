require_relative 'base'

module Bitbucket
  module WebhookEvents
    module Pullrequest
      # Pull Request Created
      class Created < Bitbucket::WebhookEvents::Pullrequest::Base
        def to_s
          "#{actor} created new PR #{pullrequest}"
        end
      end
    end
  end
end
