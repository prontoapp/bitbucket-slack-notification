require_relative 'base'

module Bitbucket
  module WebhookEvents
    module Pullrequest
      # Pull Request Merged
      class Rejected < Bitbucket::WebhookEvents::Pullrequest::Base
        def to_s
          "#{actor} rejected PR #{pullrequest}"
        end
      end
    end
  end
end
