require_relative 'pullrequest/created'
require_relative 'pullrequest/updated'
require_relative 'pullrequest/approved'
require_relative 'pullrequest/unapproved'
require_relative 'pullrequest/fulfilled'
require_relative 'pullrequest/rejected'
require_relative 'pullrequest/comment_created'
require_relative 'pullrequest/comment_updated'
require_relative 'pullrequest/comment_deleted'

module Bitbucket
  module WebhookEvents
    # Module for events in Pull Request category.
    # Pullrequest is not camelcase intentionally to match with HTTP header key
    module Pullrequest
    end
  end
end
