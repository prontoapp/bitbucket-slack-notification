module Bitbucket
  module WebhookEvents
    module Repo
      # Commit Comment Created
      # A user comments on a commit in a repository.
      class CommitCommentCreated < Bitbucket::WebhookEvent

        attr_accessor :actor, :comment, :repository, :commit

        def initialize(raw)
          super(raw)

          self.actor = Bitbucket::WebhookEntities::User.new(@raw['actor'])
          self.comment = Bitbucket::WebhookEntities::Comment.new(@raw['comment'])
          self.repository = Bitbucket::WebhookEntities::Repository.new(@raw['repository'])
          self.commit = @raw['commit']
        end

        def to_s
          "#{actor} commented on commit #{commit['hash']}: \"#{comment}\""
        end

        def commit_message
          commit['message']
        end

      end
    end
  end
end
