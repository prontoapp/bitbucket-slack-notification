module Bitbucket
  module WebhookEvents
    module Repo
      # Push
      # A user pushes 1 or more commits to a repository.
      class Push < Bitbucket::WebhookEvent
        attr_accessor :actor, :repository, :push

        def initialize(raw)
          super(raw)

          self.actor = Bitbucket::WebhookEntities::User.new(raw['actor'])
          self.repository = Bitbucket::WebhookEntities::Repository.new(raw['repository'])
          self.push = @raw['push']
        end

        def changes
          push['changes']
        end

        def to_s
          "#{actor} pushed #{changes[0]['commits'].size} commits to #{repository.full_name}"
        end
      end
    end
  end
end
