module Bitbucket
  module WebhookEvents
    module Repo
      # Fork
      # A user forks repository
      class Fork < Bitbucket::WebhookEvent
        attr_accessor :actor, :repository, :fork

        def initialize(raw)
          super(raw)

          self.actor = Bitbucket::WebhookEntities::User.new(raw['actor'])
          self.repository = Bitbucket::WebhookEntities::Repository.new(raw['repository'])
          self.fork = Bitbucket::WebhookEntities::Repository.new(raw['fork'])
        end
      end
    end
  end
end
