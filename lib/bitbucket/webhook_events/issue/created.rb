require 'json'

module Bitbucket
  module WebhookEvents
    module Issue
      # Created
      # A user creates an issue for a repository
      class Created < Bitbucket::WebhookEvent
        attr_accessor :actor, :issue, :repository

        def initialize(raw)
          @raw = raw
          self.actor = Bitbucket::WebhookEntities::User.new(@raw['actor'])
          self.issue = Bitbucket::WebhookEntities::Issue.new(@raw['issue'])
          self.repository = Bitbucket::WebhookEntities::Repository.new(@raw['repository'])
        end

        def to_s
          "#{actor} created new issue #{issue}"
        end
      end
    end
  end
end
