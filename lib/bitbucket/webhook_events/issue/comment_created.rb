require 'json'

module Bitbucket
  module WebhookEvents
    module Issue
      # Comment Created
      # A user comments on an issue associated with a repository.
      class CommentCreated < Bitbucket::WebhookEvent
        attr_accessor :actor, :issue, :repository, :comment

        def initialize(raw)
          @raw = raw
          self.actor = Bitbucket::WebhookEntities::User.new(@raw['actor'])
          self.issue = Bitbucket::WebhookEntities::Issue.new(@raw['issue'])
          self.repository = Bitbucket::WebhookEntities::Repository.new(@raw['repository'])
          self.comment = Bitbucket::WebhookEntities::Comment.new(@raw['comment'])
        end

        def to_s
          "#{actor} commented issue #{issue}: \"#{comment}\""
        end
      end
    end
  end
end
