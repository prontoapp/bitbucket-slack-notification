require 'json'

module Bitbucket
  module WebhookEvents
    module Issue
      # Updated
      # A user updated an issue for a repository
      # TODO parse changes
      class Updated < Bitbucket::WebhookEvent
        attr_accessor :actor, :issue, :repository, :comment

        def initialize(raw)
          @raw = raw
          self.actor = Bitbucket::WebhookEntities::User.new(@raw['actor'])
          self.issue = Bitbucket::WebhookEntities::Issue.new(@raw['issue'])
          self.repository = Bitbucket::WebhookEntities::Repository.new(@raw['repository'])
          self.comment = Bitbucket::WebhookEntities::Comment.new(@raw['comment'])
        end

        def changes
          @raw['changes']
        end

        def to_s
          "#{actor} updated issue #{issue} with comment \"#{comment}\""
        end
      end
    end
  end
end
