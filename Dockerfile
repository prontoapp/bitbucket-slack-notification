FROM pronto/base-ruby


# install gems
WORKDIR $HOME/bitbucket-slack-notifier
EXPOSE 80
CMD bundle exec ruby server.rb -p 80 -s Puma -o 0.0.0.0

ADD . $HOME/bitbucket-slack-notifier
RUN rm -rf .bundle
RUN bundle install --jobs 4 --retry 10 --without development

