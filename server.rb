require 'sinatra'
require 'json'
require 'slack-notifier'
require_relative 'lib/bitbucket/webhook_events'
require_relative 'lib/bitbucket_webhook_slack_formatter.rb'

slack = Slack::Notifier.new ENV['SLACK_WEBHOOK']

post '/:secret' do
  halt 403 unless params[:secret] == ENV['URL_SECRET']

  request.body.rewind
  data = request.body.read

  event = Bitbucket::WebhookEvent.parse(data, request.env['HTTP_X_EVENT_KEY'])
  puts event.class
  puts event

  event_slack_attachment = BitbucketWebhookSlackFormatter.attachment(event)

  slack.ping '', attachments: [event_slack_attachment]

  body ''
  status 200
end
